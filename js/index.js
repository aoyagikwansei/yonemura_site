// JavaScript Document
(function(){
	
	/**
	* ニュースの一部を隠す
	**/
	var hideNews = function(){
		var INITIAL_SHOWN = 4; // 最初に見せる数

		var index = 0;
		
		var $window = $(window);
		var nowY = $window.scrollTop() + $window.height();
		$('.works-wrapper *[class^=col], #works-images-container *[class^=col]').each(function(){
			var $this = $(this);
			var top = $this.offset().top;
			if(index >= INITIAL_SHOWN && nowY <= top){
				$this.attr('data-originl-top', top); // もともとの高さを保存
				setTimeout(function(){
					$this.css("display", "none");
				}, 100);
			}
			index++;
		});
	}
	
	/**
	* スクロールすると残りのニュースを表示
	**/
	var setScrollEventForNews = function(){
		var $cols = $('.works-wrapper *[class^=col], #works-images-container *[class^=col]');
		var $window = $(window);
		$window.scroll(function() {
			var nowY = $window.scrollTop() + $window.height();
   			$cols.each(function(){
				var $this = $(this);
				if(nowY > Number($this.attr('data-originl-top')) && $this.css('display') == 'none'){
					$this.css('opacity', '0').css('display', 'block').delay(500)
					.animate({
						opacity: 1
					}, {duration: 'slow', easing: 'swing'});
				}
			});
		});
	}
	
	// スマホ用グローバルメニュー表示切り替え イベント設定
	var setSPGlobalMenuEvent = function(){
		$("#show-global-menus").on("click", function(){
			var $sp_gloabl_menus = $("#sp-gloabl-menus");
			var $this = $(this);
			var $bars = $this.find('.icon-bar')
			var $batsu = $this.find('.batsu')
			
		
			if($sp_gloabl_menus.css("display") == 'none'){
				$sp_gloabl_menus.css('opacity', 0).css("display", 'block').animate({
					opacity: 1
				}, 100);
				
				
				$bars.css('display', 'none');
				$batsu.css('display', 'block');
	
			}else{
				$sp_gloabl_menus.animate({
					opacity: 0
				}, 200).css("display", 'none').css('opacity', 1);
			
				$bars.css('display', 'block');
				$batsu.css('display', 'none');
				
			}
		})
	}
	
	//  グローバルメニューに下線さ出す
	var showGlobalNaviUnderline = function(){
		var pathArray =location.pathname.split("/");
		var pageNow = pathArray[pathArray.length-1].replace(".html", "");
		
		$(".global-navi-link").each(function(){
			var $this = $(this);
			var text = $this.text();
			if(text.indexOf(pageNow) > -1){
				$this.addClass("active");
			}
		})
		
	}
	
	// スクロールをスムーズに
	var smooseJump = function() {
	  // スクロールのオフセット値
	  var offsetY = -10;
	  // スクロールにかかる時間
	  var time = 500;

	  // ページ内リンクのみを取得
	  $('a[href^=#]').click(function() {
		// 移動先となる要素を取得
		var target = $(this.hash);
		if (!target.length) return ;
		// 移動先となる値
		var targetY = target.offset().top+offsetY;
		// スクロールアニメーション
		$('html,body').animate({scrollTop: targetY}, time, 'swing');
		// ハッシュ書き換えとく
		window.history.pushState(null, null, this.hash);
		// デフォルトの処理はキャンセル
		return false;
	  });
	}
	
	/*
	メイン画像のカルーセルの左右移動
	*/
	var setCarousel = function(){
		var $carousel = $("#carousel1");
		$carousel.carousel({
			wrap: true,
			ride: false,
			interval: false
		})
		$carousel.find(".right").on("click", function(){
			$carousel.carousel('next');
		})
		$carousel.find(".left").on("click", function(){
			$carousel.carousel('prev');
		})
	}
	
	/* 
	スクロール量が0のときヘッダーを大きく
	*/
	var setZeroscrollNavi = function(){
		
		var sliding = false;
		$('#carousel1').on('slide.bs.carousel', function () {
		  sliding = true;
		}).on('slid.bs.carousel', function () {
		  sliding = false;
		})
		
		var $globalNavi = $("#global-navi");
		var $mainImageContainer = $("#index-main-image-container");
		$(window).on("scroll", function(){
			if(!sliding){
				if($(this).scrollTop() < 2){
				$globalNavi.addClass("zeroscroll")
				$mainImageContainer.addClass("zeroscroll")
			}else{
				$globalNavi.removeClass("zeroscroll")
				$mainImageContainer.removeClass("zeroscroll")
			}
			}
			
		})
		
	}
	
	var setHighlightMenu = function(){
		$.each(["works", "about", "news", "contact", "recruit"], function(index, name){
			if(location.pathname.includes(name)){
				var $link = $("a[href=\""+name+".html\"]").first();
				$link.parent().addClass("active");
				var $span = $link.find("span").first();
				$link.empty();
				$link.append($("<h1 />").text($span.text()));
			}
		})
		
		
	}
	
	
	//hideNews();
	$(document).ready(function(){
		$("img").lazyload({
			effect : "fadeIn"
		});
		
		//hideNews();
		setScrollEventForNews();
		setSPGlobalMenuEvent();
		//showGlobalNaviUnderline();
		smooseJump();
		setCarousel();
		setZeroscrollNavi();
		
		setHighlightMenu();
	});
})()
