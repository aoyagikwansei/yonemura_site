<?php 
mb_language('uni');
mb_internal_encoding('UTF-8');
$address = $_POST['mail_address'];
$name = $_POST["name"];
$subject =  $_POST["subject"];
$body2 = $_POST["body"];
$body = <<< EOF
 お問合わせがありました。
 メールアドレス: $address
 氏名: $name
 件名: $subject
 本文: $body2
EOF;
//mb_send_mail("info@yoneda-arch.com", mb_convert_encoding("テスト", "UTF-8"),  mb_convert_encoding($body, "UTF-8"));
mb_send_mail("info@yoneda-arch.com", mb_convert_encoding("お問い合わせがありました｜YONEDA MASAHIRO ARCHITECTS", "UTF-8"),  mb_convert_encoding($body, "UTF-8"));

$subject2 = "お問い合わせを受け付けました｜YONEDA MASAHIRO ARCHITECTS";
$body3 = <<< EOF
お問い合わせありがとうございます。

以下の内容でお問い合わせを受け付けました。
$name 様
$subject
　　　
$body2
EOF;
mb_send_mail($address, mb_convert_encoding($subject2, "UTF-8"),  mb_convert_encoding($body3, "UTF-8"));
?>
<!DOCTYPE html>
<html lang="en"><!-- InstanceBegin template="/Templates/default.dwt" codeOutsideHTMLIsLocked="false" -->
  <head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- InstanceBeginEditable name="doctitle" -->
	<title>YONEDA MASAHIRO ARCHITECTS</title>
	<!-- InstanceEndEditable -->
  <meta name="description" content="米田　真大 Yoneda Masahiro のWebサイトです。">
  <meta name="keywords" content="米田　真大 Yoneda Masahiro 建築 Architecture">
  <!-- Bootstrap -->
  <link href="css/bootstrap.css" rel="stylesheet">
  <link href="css/style.css" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans|Roboto" rel="stylesheet">
  <meta name="format-detection" content="telephone=no">
  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
  <!-- InstanceBeginEditable name="head" -->
  <!-- InstanceEndEditable -->
  </head>
  <body>
  <!--グローバルナビ-->
  <div class='container-fluid zeroscroll' id='global-navi' >
  	<div class="container" >
  		<div class="row">
		  <div class="col-xs-10 col-sm-5">
			<a href='index.html' class='global-navi-top' >
				<img src='images/samples/logo-long.png' />
			</a>
		  </div>
		<div class="col-xs-2 col-sm-7" >
			<button type="button" class="pull-right hidden-md hidden-lg hidden-xl" id='show-global-menus'>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="batsu" >x</span>
			</button>
			<ul class="global-navi-wrapper hidden-xs hidden-sm"  >
			  <li class="global-navi-col">
				<a href='news.html' class='global-navi-link'  >
					<span>
						NEWS
					</span>
				</a>
			  </li>
			   <li class="global-navi-col">
				<a href='works.html' class='global-navi-link'  >
					
					<span>WORKS</span>
				</a>
			  </li>
			   <li class="global-navi-col">
				<a href='about.html' class='global-navi-link'  >
					
					<span>ABOUT</span>
				</a>
			  </li>
			   
			   <li class="global-navi-col">
				<a href='contact.html' class='global-navi-link'  >
					
					<span>CONTACT</span>
				</a>
			  </li>
			   <li class="global-navi-col">
				<a href='recruit.html' class='global-navi-link'  >
					
					<span>RECRUIT</span>
				</a>
			  </li>

			</ul>
			
			
		</div>
    </div>
    <div class='row hidden-md hidden-lg' id='sp-gloabl-menus'>
    	<div class='col-xs-12' >
    		<ul class=""  >
			  <li >
				<a href='news.html' class='global-navi-link'  >
					<span>
						NEWS
					</span>
				</a>
			  </li>
			   <li >
				<a href='works.html' class='global-navi-link'  >
					
					<span>WORKS</span>
				</a>
			  </li>
			   <li >
				<a href='about.html' class='global-navi-link'  >
					
					<span>ABOUT</span>
				</a>
			  </li>
			    <li >
				<a href='contact.html' class='global-navi-link'  >
					
					<span>CONTACT</span>
				</a>
			  </li>
			   <li class="global-navi-col">
				<a href='recruit.html' class='global-navi-link'  >
					
					<span>RECRUIT</span>
				</a>
			  </li>
			</ul>
    	</div>
    </div>
  	</div>
	
  </div>
<!--グローバルナビ終わり-->

<!-- InstanceBeginEditable name="EditRegionMain" -->
<article id="main">
	<div class="container" >
		<div class="row" >
			<div class="col-xs-12" style="  margin: 200px 0;" >
				お問い合わせを承りました。	
			</div>
		</div>
	</div>
</article>
<!-- InstanceEndEditable -->

<!--フッター-->
<footer id="footer">
		<div class="" >
			<div class=' footer-top'>
				<div class=''>
					<div class="" >
						
						<ul class="pull-left">
							<li>
								<a href='/' >HOME</a> |
							</li>
							<li>
								<a href='/works.html' >WORKS</a>  |
							</li>
							<li>
								<a href='/about.html' >ABOUT</a> |
							</li>
							<li>
								<a href='/recruit.html' >RECRUIT</a> | 
							</li>
							<li>
								<a href='/contact.html' >CONTACT</a>
							</li>
						</ul>
						<div class='pull-right pull-left-xs' style="margin-bottom: 10px;" >

							E-MAIL : <img src='images/mailaddress.png' style='height: 14px;'/>
						</div>
						
						
					</div>

				</div>
			</div>
			<div class=' footer-bottom' style="margin-top: 10px;">
				<div class=''>
					<div class="" >
						
						<div class='pull-right pull-left-xs'>
							©2017 YONEDA MASAHIRO ARCHITECTS 
						</div>
					</div>

				</div>
			</div>
		</div>
		
	
</footer>
<!--フッタ終わりー-->
 
 	<!-- 下に行くボタン -->
 	<div>
		<a href="#footer" class="go-bottom-button" ></a>
	</div>
    <!-- 下に行くボタン終わり -->
  
  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
	<script src="js/jquery-1.11.3.min.js"></script>

	<!-- Include all compiled plugins (below), or include individual files as needed --> 
	<script src="js/bootstrap.js"></script>
	
	<script src="js/index.js"></script>
	<script src="js/jquery.lazyload.min.js"></script>
  </body>
<!-- InstanceEnd --></html>